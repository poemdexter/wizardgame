﻿using UnityEngine;
using System.Collections;

public class ArmColorHandler : MonoBehaviour
{
    [System.Serializable]
    public class ArmSprite
    {
        public Characters playerId;
        public Sprite armSprite;
    }

    public ArmSprite[] armSprites;

    private void SetPlayer(Characters id)
    {
        GetComponent<SpriteRenderer>().sprite = GetArmSprite(id);
    }

    private Sprite GetArmSprite(Characters id)
    {
        for (int i = 0; i < armSprites.Length; i++)
        {
            if (armSprites[i].playerId == id)
                return armSprites[i].armSprite;
        }
        return null; 
    }

    public void HideArmSprite()
    {
        GetComponent<SpriteRenderer>().sprite = null;
    }
}