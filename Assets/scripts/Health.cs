﻿using UnityEngine;
using System.Collections;

public class Health : MonoBehaviour
{
    public PlayerID playerId;
    public int HP;
    private Animator anim;
    private GameManager gameManager;

    private void Start()
    {
        anim = GetComponent<Animator>();
        gameManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
    }

    public void GetHurt()
    {
        if (--HP == 0)
        {
            Die();
        }
    }

    private void Die()
    {
        anim.SetTrigger("Die");
        transform.FindChild("arm").GetComponent<ArmColorHandler>().HideArmSprite();
        gameManager.KillPlayer(playerId);
    }

    private void SetPlayer(PlayerID id)
    {
        playerId = id;
    }
}