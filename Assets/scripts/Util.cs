﻿using UnityEngine;
using System.Collections;

public enum PlayerID
{
    None,
    P1,
    P2,
    P3,
    P4
}

public enum Buttons
{
    A,
    X,
    B
}

public enum WandType
{
    None,
    Ice,
    Fire
}

public enum Characters
{
    Green,
    Blue,
    Red,
    Brown,
    Undead
}

public enum AimDirection
{
    None,
    North,
    Northeast,
    East,
    Southeast,
    South,
    Southwest,
    West,
    Northwest
}

public class Util : MonoBehaviour
{

    private void Awake()
    {
        Screen.showCursor = false;
    }

    private void Update()
    {
        if (Input.GetButtonDown("RESET"))
        {
            Application.Quit();
        }
    }

    public static Vector2 ConvertAimDirToVector2(AimDirection direction)
    {
        switch (direction)
        {
            case AimDirection.North:
                return new Vector2(0f, 1f);
            case AimDirection.Northeast:
                return new Vector2(.707f, .707f);
            case AimDirection.East:
                return new Vector2(1f, 0f);
            case AimDirection.Southeast:
                return new Vector2(.707f, -.707f);
            case AimDirection.South:
                return new Vector2(0f, -1f);
            case AimDirection.Southwest:
                return new Vector2(-.707f, -.707f);
            case AimDirection.West:
                return new Vector2(-1f, 0f);
            case AimDirection.Northwest:
                return new Vector2(-.707f, .707f);
            default:
                return Vector2.zero;
        }
    }

    public static AimDirection ConvertVector2ToAimDir(Vector2 direction)
    {
        if (direction.x > .7f && direction.y < -.7f)
        {
            return AimDirection.Southeast;
        }
        if (direction.x > .7f && direction.y == 0f)
        {
            return AimDirection.East;
        }
        if (direction.x > .7f && direction.y > .7f)
        {
            return AimDirection.Northeast;
        }
        if (direction.x == 0f && direction.y < -.7f)
        {
            return AimDirection.South;
        }
        if (direction.x == 0f && direction.y > .7f)
        {
            return AimDirection.North;
        }
        if (direction.x < -.7f && direction.y < -.7f)
        {
            return AimDirection.Southwest;
        }
        if (direction.x < -.7f && direction.y == 0f)
        {
            return AimDirection.West;
        }
        if (direction.x < -.7f && direction.y > .7f)
        {
            return AimDirection.Northwest;
        }
        return AimDirection.None;
    }
}