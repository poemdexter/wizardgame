﻿using UnityEngine;
using System.Collections;

public class HandleGetHit : MonoBehaviour
{
    private Health health;
    private Buffable buffable;

    private void Start()
    {
        health = GetComponent<Health>();
        buffable = transform.FindChild("buff").GetComponent<Buffable>();
    }

    public void InteractWithOthersBuff(WandType buffType)
    {
        if (buffable.IsBuffed())
        {
            WandType buff = buffable.GetBuffType();

            switch (buff)
            {
                case WandType.Fire:
                    InteractWhileBuff_Fire(buffType);
                    break;
                case WandType.Ice:
                    InteractWhileBuff_Ice(buffType);
                    break;
            }
        }
        else
        {
            InteractWithoutBuff(buffType);
        }
    }

    public void GetHitByProjectile(WandType projType)
    {
        if (buffable.IsBuffed())
        {
            WandType buff = buffable.GetBuffType();

            switch (buff)
            {
                case WandType.Fire:
                    ProjectileHitWhileBuff_Fire(projType);
                    break;
                case WandType.Ice:
                    ProjectileHitWhileBuff_Ice(projType);
                    break;
            }
        }
        else
        {
            ProjectileHitWithoutBuff(projType);
        }
    }

    private void ProjectileHitWithoutBuff(WandType projType)
    {
        health.GetHurt(); // take damage
    }

    private void InteractWithoutBuff(WandType buffType)
    {
        health.GetHurt(); // take damage
    }

    private void InteractWhileBuff_Fire(WandType buffType)
    {
        switch (buffType)
        {
            case WandType.Fire:
                // firewalkers can walk over each other's fire
                break;
            case WandType.Ice:
                buffable.LoseBuff();
                break;
        }
    }

    private void InteractWhileBuff_Ice(WandType buffType)
    {
        switch (buffType)
        {
            case WandType.Fire:
                buffable.LoseBuff();
                break;
            case WandType.Ice:
                // ice shields don't interact at moment
                break;
        }
    }

    private void ProjectileHitWhileBuff_Fire(WandType projType)
    {
        switch (projType)
        {
            case WandType.Fire:
                health.GetHurt();
                break;
            case WandType.Ice:
                buffable.LoseBuff();
                break;
        }
    }

    private void ProjectileHitWhileBuff_Ice(WandType projType)
    {
        switch (projType)
        {
            case WandType.Fire:
                buffable.LoseBuff();
                break;
            case WandType.Ice:
                health.GetHurt();
                break;
        }
    }
}