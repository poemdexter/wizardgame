﻿using UnityEngine;
using System.Collections;

public class FireWand : MonoBehaviour
{
    public PlayerID playerId;
    public Buttons fireButton;
    public float projectileOffset;

    private int playerid;
    private string buttonid;
    private bool firePressed, canFire;

    private WandManager wandManager;
    private Movement movement;
    private ProjectileManager projectileManager;
    private GameManager gameManager;
    private Status status;

    private void Awake()
    {
        playerid = (int) playerId;
        buttonid = fireButton.ToString();
        canFire = true;
    }

    private void Start()
    {
        wandManager = transform.parent.GetComponent<WandManager>();
        movement = transform.parent.GetComponent<Movement>();
        status = transform.parent.GetComponent<Status>();
        gameManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
        projectileManager = GameObject.FindGameObjectWithTag("ProjectileManager").GetComponent<ProjectileManager>();
    }

    private void Update()
    {
        CheckInput();

        // fire the wand
        if (firePressed)
        {
            firePressed = false;

            if (status.CurrentEffect != StatusEffect.Stunned && gameManager.HasRoundStarted() && gameManager.IsPlayerAlive(playerId) && wandManager.IsWandEquipped() && wandManager.TryToUseWand())
            {
                DoFire();
            }
        }
    }

    // fire wand out
    private void DoFire()
    {
        // go local right after rotation flipped if person going left with offset
        Vector3 fireVector3;
        if (transform.parent.localScale.x >= .9f && transform.parent.localScale.x <= 1.1f) // == 1
        {
            fireVector3 = transform.position + transform.right * projectileOffset;
        }
        else
        {
            fireVector3 = transform.position + (-transform.right * projectileOffset);
            if (movement.GetAimDirection() == AimDirection.Northwest || movement.GetAimDirection() == AimDirection.North)
            {
                fireVector3 += 1.44f * Vector3.up * projectileOffset;
            }
            if (movement.GetAimDirection() == AimDirection.Southwest || movement.GetAimDirection() == AimDirection.South)
            {
                fireVector3 += 1.44f * -Vector3.up * projectileOffset;
            }
        }
        GameObject projectile = (GameObject) Instantiate(projectileManager.GetProjectile(wandManager.GetWandType()), fireVector3, Quaternion.identity);
        Projectile p = projectile.GetComponent<Projectile>();
        p.SetAimDirection(movement.GetAimDirection());
        p.SetShooter(playerId);
    }

    private void CheckInput()
    {
        if (gameManager.IsPlayerAlive(playerId))
        {
            if (canFire && Input.GetButtonDown(buttonid + "_" + playerid))
            {
                firePressed = true;
                canFire = false;
            }
            else if (Input.GetButtonUp(buttonid + "_" + playerid))
            {
                canFire = true;
            }
        }
    }

    private void SetPlayer(PlayerID id)
    {
        playerId = id;
        playerid = (int) playerId;
    }
}