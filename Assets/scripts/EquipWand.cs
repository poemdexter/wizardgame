﻿using UnityEngine;
using System.Collections;

public class EquipWand : MonoBehaviour
{
    public PlayerID playerId;
    public Buttons equipButton;

    private int playerid;
    private string buttonid;
    private bool canEquip; // button not pressed
    private bool equipPressed; // button press not handled
    private bool isOverWand;

    private WandManager wandManager;
    private Wand wand;

    private void Awake()
    {
        playerid = (int) playerId;
        buttonid = equipButton.ToString();
        canEquip = true;
    }

    private void Start()
    {
        wandManager = GetComponent<WandManager>();
    }

    private void Update()
    {
        CheckInput();

        // equip the wand or throw the wand
        if (equipPressed && isOverWand && wand != null)
        {
            wandManager.PickUpWand(wand);
            isOverWand = false;
        }
        else if (equipPressed && !isOverWand && wandManager.IsWandEquipped())
        {
            wandManager.ThrowWand();
        }

        equipPressed = false;
    }

    private void CheckInput()
    {
        if (canEquip && Input.GetButtonDown(buttonid + "_" + playerid))
        {
            equipPressed = true;
            canEquip = false;
        }
        else if (Input.GetButtonUp(buttonid + "_" + playerid))
        {
            canEquip = true;
        }
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag("Wand"))
        {
            wand = col.GetComponent<Wand>();
            if (wand.ToBeDestroyed())
            {
                isOverWand = false;
                wand = null;
            }
            else
            {
                isOverWand = true;
            }
        }
    }

    private void OnTriggerExit2D(Collider2D col)
    {
        if (col.CompareTag("Wand"))
        {
            isOverWand = false;
            wand = null;
        }
    }

    private void SetPlayer(PlayerID id)
    {
        playerId = id;
        playerid = (int) playerId;
    }
}