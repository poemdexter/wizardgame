﻿using UnityEngine;
using System.Collections;

public class Buffable : MonoBehaviour
{
    private WandType buffType;
    private GameObject currentBuffGO;
    private Buff currentBuff;
    private BuffManager buffManager;

    private void Awake()
    {
        buffType = WandType.None;
    }

    private void Start()
    {
        buffManager = GameObject.FindGameObjectWithTag("BuffManager").GetComponent<BuffManager>();
    }

    public bool IsBuffed()
    {
        return buffType != WandType.None;
    }

    public WandType GetBuffType()
    {
        return buffType;
    }

    public void AttemptBuff(WandType type)
    {
        // remove old one
        LoseBuff();
        
        // add new one
        buffType = type;
        currentBuffGO = (GameObject) Instantiate(buffManager.GetBuff(buffType));
        currentBuffGO.transform.parent = transform.parent;
        currentBuff = currentBuffGO.GetComponent<Buff>();
        currentBuff.ApplyBuff(gameObject);
    }

    public void LoseBuff()
    {
        if (currentBuff != null)
        {
            buffType = WandType.None;
            currentBuff.LoseBuff(gameObject);
            currentBuff = null;
            Destroy(currentBuffGO);
        }
    }
}