﻿using UnityEngine;

public class WandStun : MonoBehaviour
{
    public PlayerID playerId;
    private Status status;
    private void OnStart()
    {
        status = GetComponent<Status>();
    }

    // if hit by a wand get stunned
    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag("Wand"))
        {
            Wand wand = col.GetComponent<Wand>();
            if (wand.IsThrown)
            {
                if (status == null)
                {
                    status = GetComponent<Status>();
                }
                Debug.Log("stunned player");
                if (wand.Thrower != playerId)
                {
                    status.GetStunned();
                    Destroy(wand.gameObject);
                }
            }
        }
    }

    private void SetPlayer(PlayerID id)
    {
        playerId = id;
    }
}