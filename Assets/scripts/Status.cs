﻿using UnityEngine;
using System.Collections;

public enum StatusEffect
{
    Normal,
    Stunned,
    IceShield,
    FireShield
}

public class Status : MonoBehaviour
{
    public StatusEffect CurrentEffect { get; set; }
    public float stunTime;
    private float currentStunTime;
    private Animator anim;

    private void Awake()
    {
        CurrentEffect = StatusEffect.Normal;
    }

    private void Start()
    {
        anim = GetComponent<Animator>();
    }

    public void GetStunned()
    {
        switch (CurrentEffect)
        {
            case StatusEffect.Normal:
                CurrentEffect = StatusEffect.Stunned;
                anim.SetBool("Stunned", true);
                break;
        }
    }

    private void Update()
    {
        if (CurrentEffect == StatusEffect.Stunned)
        {
            if ((currentStunTime += Time.deltaTime) >= stunTime)
            {
                currentStunTime = 0;
                CurrentEffect = StatusEffect.Normal;
                anim.SetBool("Stunned", false);
            }
        }
    }
}