﻿using System.Net;
using UnityEngine;
using System.Collections;

public class Movement : MonoBehaviour
{
    public PlayerID playerId;
    public Buttons rollButton;
    public float moveSpeed;
    public float rollSpeed;
	public float rollDelay;
    public float BuffSpeed { get; set; }

    private int playerid;
    private string rollButtonid;
    private bool up, down, left, right;
	private bool canMoveUp, canMoveDown, canMoveLeft, canMoveRight;
	private bool isRolling;
	private bool hasJustRolled = false;
    private Animator anim;
    private AimDirection aimDirection;
    private bool[] rollDirection = new bool[4];
    private AimDirection rollAimDirection;
    private Transform arm;
    private WandManager wandManager;
    private Status status;
    private GameManager gameManager;
	private float currentTime;

    private void Awake()
    {
        playerid = (int) playerId;
        rollButtonid = rollButton.ToString();
        aimDirection = AimDirection.None;
        canMoveUp = true;
        canMoveDown = true;
        canMoveLeft = true;
        canMoveRight = true;
    }

    private void Start()
    {
        wandManager = GetComponent<WandManager>();
        gameManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
        anim = GetComponent<Animator>();
        status = GetComponent<Status>();
        arm = transform.GetChild(0);
    }

    private void Update()
    {
        CheckInput();
         
        if (up || down || left || right)
        {
            anim.SetBool("Walking", true);
        }
        else
        {
            anim.SetBool("Walking", false);
        }

        // determine move directions
        float udDirection = 0f;
        float lrDirection = 0f;
        if (up || down)
        {
            udDirection = (up) ? 1f : -1f;
        }
        if (left || right)
        {
            lrDirection = (right) ? 1f : -1f;
            transform.localScale = new Vector3(lrDirection, 1f, 1f);
        }

        // determine arm placement
        AimDirection dir = Util.ConvertVector2ToAimDir(new Vector2(lrDirection, udDirection));
        if (dir != AimDirection.None)
        {
            aimDirection = dir;
            RotateArm(dir);
        }

        AimDirection moveDir = GetMoveDirection(up, down, left, right);

        // check if rolling
        if (!hasJustRolled && !isRolling && !wandManager.IsWandEquipped() && Input.GetButtonDown(rollButtonid + "_" + playerid))
        {
            isRolling = true;
            anim.SetTrigger("Rolling");
            rollDirection[0] = up;
            rollDirection[1] = down;
            rollDirection[2] = left;
            rollDirection[3] = right;
        }

        //check roll delay
        if (hasJustRolled && (currentTime += Time.deltaTime) > rollDelay)
        {
            hasJustRolled = false;
            currentTime = 0;
        }

        if (status.CurrentEffect != StatusEffect.Stunned && gameManager.IsPlayerAlive(playerId) && gameManager.HasRoundStarted())
        {
            if (!isRolling) // we're walking
            {
                // determine translation
                Vector2 moveVector = Util.ConvertAimDirToVector2(moveDir) * (moveSpeed + BuffSpeed) * Time.deltaTime;
                transform.Translate(moveVector);
            }
            else // we're rolling
            {
                // get aim direction based off saved u/d/l/r from start of roll.  this prevents us going through wall.
                rollAimDirection = GetMoveDirection(rollDirection[0], rollDirection[1], rollDirection[2], rollDirection[3]);
                Vector2 moveVector = Util.ConvertAimDirToVector2(rollAimDirection) * rollSpeed * Time.deltaTime;
                transform.Translate(moveVector);
            }
        }
        else
        {
            anim.SetBool("Walking", false); // no moving while game ended or player dead
        }
    }

    private void RotateArm(AimDirection dir)
    {
        switch (dir)
        {
            case AimDirection.North:
                arm.localRotation = Quaternion.Euler(new Vector3(0f, 0f, 90f));
                break;
            case AimDirection.Northeast:
                arm.localRotation = Quaternion.Euler(new Vector3(0f, 0f, 45f));
                break;
            case AimDirection.East:
                arm.localRotation = Quaternion.Euler(new Vector3(0f, 0f, 0f));
                break;
            case AimDirection.Southeast:
                arm.localRotation = Quaternion.Euler(new Vector3(0f, 0f, -45f));
                break;
            case AimDirection.South:
                arm.localRotation = Quaternion.Euler(new Vector3(0f, 0f, -90f));
                break;
            case AimDirection.Southwest:
                arm.localRotation = Quaternion.Euler(new Vector3(0f, 0f, -45f));
                break;
            case AimDirection.West:
                arm.localRotation = Quaternion.Euler(new Vector3(0f, 0f, 0f));
                break;
            case AimDirection.Northwest:
                arm.localRotation = Quaternion.Euler(new Vector3(0f, 0f, 45f));
                break;
            default:
                break;
        }
    }

    private void CheckInput()
    {
        if (CheckRoundStarted() && gameManager.IsPlayerAlive(playerId))
        {
            if (Input.GetAxisRaw("L_YAxis_" + playerid) < 0)
            {
                up = true;
            }
            else if (Input.GetAxisRaw("L_YAxis_" + playerid) == 0)
            {
                up = false;
            }
            if (Input.GetAxisRaw("L_YAxis_" + playerid) > 0)
            {
                down = true;
            }
            else if (Input.GetAxisRaw("L_YAxis_" + playerid) == 0)
            {
                down = false;
            }
            if (Input.GetAxisRaw("L_XAxis_" + playerid) < 0)
            {
                left = true;
            }
            else if (Input.GetAxisRaw("L_XAxis_" + playerid) == 0)
            {
                left = false;
            }
            if (Input.GetAxisRaw("L_XAxis_" + playerid) > 0)
            {
                right = true;
            }
            else if (Input.GetAxisRaw("L_XAxis_" + playerid) == 0)
            {
                right = false;
            }
        }
    }

    public void DoneRolling()
    {
        isRolling = false;
		hasJustRolled = true;
    }

    public AimDirection GetAimDirection()
    {
        return aimDirection;
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag("Wall"))
        {
            switch (col.name)
            {
                case "topwall":
                    canMoveUp = false;
                    break;
                case "bottomwall":
                    canMoveDown = false;
                    break;
                case "leftwall":
                    canMoveLeft = false;
                    break;
                case "rightwall":
                    canMoveRight = false;
                    break;
            }
        }
    }

    private void OnTriggerExit2D(Collider2D col)
    {
        if (col.CompareTag("Wall"))
        {
            switch (col.name)
            {
                case "topwall":
                    canMoveUp = true;
                    break;
                case "bottomwall":
                    canMoveDown = true;
                    break;
                case "leftwall":
                    canMoveLeft = true;
                    break;
                case "rightwall":
                    canMoveRight = true;
                    break;
            }
        }
    }

    private AimDirection GetMoveDirection(bool u, bool d, bool l, bool r)
    {
        bool u1, d1, l1, r1;
        u1 = u && canMoveUp;
        d1 = d && canMoveDown;
        l1 = l && canMoveLeft;
        r1 = r && canMoveRight;

        // determine move directions
        float udDirection = 0f;
        float lrDirection = 0f;
        if (u1 || d1)
        {
            udDirection = (u1) ? 1f : -1f;
        }
        if (l1 || r1)
        {
            lrDirection = (r1) ? 1f : -1f;
        }

        // determine arm placement
        return Util.ConvertVector2ToAimDir(new Vector2(lrDirection, udDirection));
    }

    private bool CheckRoundStarted()
    {
        return gameManager.HasRoundStarted();
    }

    private void SetPlayer(PlayerID id)
    {
        playerId = id;
        playerid = (int) playerId;
    }
}