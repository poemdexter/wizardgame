﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour
{
    public WandType wandType;
    public float moveSpeed;
    private AimDirection aimDirection;
    private Vector2 moveDirection;
    private PlayerID shooter;

    private void Update()
    {
        transform.Translate(moveDirection * moveSpeed * Time.deltaTime);
    }

    public void SetAimDirection(AimDirection direction)
    {
        aimDirection = direction;
        moveDirection = Util.ConvertAimDirToVector2(aimDirection);
    }

    public void SetShooter(PlayerID id)
    {
        shooter = id;
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        // only destroy bullet if player isn't shooter and has > 0 hp
        if (col.CompareTag("Player"))
        {
            Health hp = col.GetComponent<Health>();
            if (hp != null && hp.playerId != shooter && hp.HP > 0)
            {
                col.GetComponent<HandleGetHit>().GetHitByProjectile(wandType);
                Destroy(gameObject);
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D col)
    {
        if (col.collider.CompareTag("Wall"))
        {
            Destroy(gameObject);
        }
    }
}