﻿using UnityEngine;
using System.Collections;

public class IceBuff : Buff
{
    public Sprite iceShieldSprite;
    public float shieldTime;
    private float currentTime;
    private bool ended;
    private GameObject player;

    public override void ApplyBuff(GameObject gameObject)
    {
        ended = false;
        player = gameObject;
        player.GetComponent<SpriteRenderer>().sprite = iceShieldSprite;
    }

    public override void LoseBuff(GameObject gameObject)
    {
        player.GetComponent<SpriteRenderer>().sprite = null;
    }

    private void Update()
    {
        if (!ended)
        {
            if ((currentTime += Time.deltaTime) >= shieldTime)
            {
                ended = true;
                player.GetComponent<Buffable>().LoseBuff();
            }
        }
    }
}