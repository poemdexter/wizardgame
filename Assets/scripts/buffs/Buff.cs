﻿using UnityEngine;
using System.Collections;

public abstract class Buff : MonoBehaviour
{
    public abstract void ApplyBuff(GameObject gameObject);
    public abstract void LoseBuff(GameObject gameObject);
}
