﻿using UnityEngine;
using System.Collections;

public class FireBuff : Buff
{
    public GameObject firewalkPrefab;
    public float walkTime;
    public float walkBoostSpeed;
    private float currentTime;
    private bool ended;
    private GameObject player;
    public float fireSpawnRate;
    private float currentSpawnTime;

    public override void ApplyBuff(GameObject gameObject)
    {
        ended = false;
        player = gameObject;
        player.transform.parent.GetComponent<Movement>().BuffSpeed = walkBoostSpeed;
    }

    public override void LoseBuff(GameObject gameObject)
    {
        player.transform.parent.GetComponent<Movement>().BuffSpeed = 0;
    }

    private void Update()
    {
        if (!ended)
        {
            // spawn fires where player walks
            if ((currentSpawnTime += Time.deltaTime) >= fireSpawnRate)
            {
                currentSpawnTime = 0;
                GameObject go = (GameObject) Instantiate(firewalkPrefab, player.transform.position, Quaternion.identity);
                go.GetComponent<TouchFireWalk>().Player = player.transform.parent.GetComponent<Health>().playerId;  // owner of firewalking
            }

            if ((currentTime += Time.deltaTime) >= walkTime)
            {
                ended = true;
                player.GetComponent<Buffable>().LoseBuff();
            }
        }
    }
}