﻿using System;
using UnityEngine;

public class Wand : MonoBehaviour
{
    public WandType Type { get; set; }
    public float throwDistance;
    public float rotationPower;
    public float destroyTime;
    public float despawnTime;
    private Vector2 moveDirection;
    public bool IsThrown { get; private set; }
    public PlayerID Thrower { get; private set; }
    private float currentTime;
    private bool toBeDestroyed;

    private void Awake()
    {
        Type = GetRandomWandType();
    }

    private WandType GetRandomWandType()
    {
        Array types = Enum.GetValues(typeof (WandType));
        return (WandType) types.GetValue(UnityEngine.Random.Range(1, types.Length));
    }

    private void Start()
    {
        if (!IsThrown)
            SetParticles();
    }

    private void SetParticles()
    {
        GameObject wpm = GameObject.FindGameObjectWithTag("WandParticleManager");
        ParticleSystem p = wpm.GetComponent<WandParticleManager>().GetWandParticles(Type);
        var go = (ParticleSystem) Instantiate(p, transform.GetChild(0).position, p.transform.rotation);
        go.transform.parent = transform;
    }

    public void GetThrown(AimDirection direction, PlayerID thrower)
    {
        moveDirection = Util.ConvertAimDirToVector2(direction);
        rigidbody2D.AddForce(moveDirection * throwDistance);
        rigidbody2D.AddTorque(rotationPower);
        toBeDestroyed = true;
        IsThrown = true;
        Thrower = thrower;
    }

    public bool ToBeDestroyed()
    {
        return toBeDestroyed;
    }

    private void Update()
    {
        if (toBeDestroyed) // thrown wand ready to die
        {
            if ((currentTime += Time.deltaTime) >= destroyTime)
            {
                Destroy(gameObject);
            }
        }
        else // spawned ready to despawn
        {
            if ((currentTime += Time.deltaTime) >= despawnTime)
            {
                Destroy(gameObject);
            }
        }
    }
}