﻿using UnityEngine;
using System.Collections;

public class CastWand : MonoBehaviour
{
    public PlayerID playerId;
    public Buttons castButton;

    private int playerid;
    private string buttonid;
    private bool castPressed, canCast;

    private WandManager wandManager;
    private Buffable buffable;

    private void Awake()
    {
        playerid = (int) playerId;
        buttonid = castButton.ToString();
        canCast = true;
    }


    private void Start()
    {
        wandManager = GetComponent<WandManager>();
        buffable = transform.FindChild("buff").GetComponent<Buffable>();
    }

    private void Update()
    {
        CheckInput();

        // cast the wand
        if (castPressed)
        {
            castPressed = false;
            if (wandManager.IsWandEquipped() && wandManager.TryToUseWand())
            {
                DoCast();
            }
        }
    }

    // cast wand on self
    private void DoCast()
    {
        buffable.AttemptBuff(wandManager.GetWandType());
    }

    private void CheckInput()
    {
        if (playerid != 0)
        {
            if (canCast && Input.GetButtonDown(buttonid + "_" + playerid))
            {
                castPressed = true;
                canCast = false;
            }
            else if (Input.GetButtonUp(buttonid + "_" + playerid))
            {
                canCast = true;
            }
        }
    }

    private void SetPlayer(PlayerID id)
    {
        playerId = id;
        playerid = (int) playerId;
    }
}