﻿using System;
using UnityEngine;
using System.Collections;

public class TitleManager : MonoBehaviour
{
    private bool p1Playing, p2Playing, p3Playing, p4Playing;
    private bool p1Ready, p2Ready, p3Ready, p4Ready;
    private int playersReady = 0;
    private bool readyToStart = false;
    private float currentStartTime;
    private GlobalManager globalManager;
    public float secondsUntilStart;
    public UILabel p1ReadyLabel, p2ReadyLabel, p3ReadyLabel, p4ReadyLabel;
    public UI2DSprite p1ButtonSprite, p2ButtonSprite, p3ButtonSprite, p4ButtonSprite;
    public UI2DSprite p1CharSprite, p2CharSprite, p3CharSprite, p4CharSprite;
    public UIPanel startPanel;
    private int p1CharChoice = 0, p2CharChoice = 1, p3CharChoice = 2, p4CharChoice = 3;
    private bool p1Swapped, p2Swapped, p3Swapped, p4Swapped;

    [System.Serializable]
    public class Character
    {
        public Characters type;
        public Sprite sprite;
    }

    public Character[] characters;

    public Sprite GetRightCharacterSprite(PlayerID player)
    {
        switch (player)
        {
            case PlayerID.P1:
                if (p1CharChoice + 1 == characters.Length)
                {
                    p1CharChoice = 0;
                    globalManager.SetPlayerCharacter(PlayerID.P1, characters[0].type);
                    return characters[0].sprite;
                }
                else
                {
                    p1CharChoice++;
                    globalManager.SetPlayerCharacter(PlayerID.P1, characters[p1CharChoice].type);
                    return characters[p1CharChoice].sprite;
                }
            case PlayerID.P2:
                if (p2CharChoice + 1 == characters.Length)
                {
                    p2CharChoice = 0;
                    globalManager.SetPlayerCharacter(PlayerID.P2, characters[0].type);
                    return characters[0].sprite;
                }
                else
                {
                    p2CharChoice++;
                    globalManager.SetPlayerCharacter(PlayerID.P2, characters[p2CharChoice].type);
                    return characters[p2CharChoice].sprite;
                }
            case PlayerID.P3:
                if (p3CharChoice + 1 == characters.Length)
                {
                    p3CharChoice = 0;
                    globalManager.SetPlayerCharacter(PlayerID.P3, characters[0].type);
                    return characters[0].sprite;
                }
                else
                {
                    p3CharChoice++;
                    globalManager.SetPlayerCharacter(PlayerID.P3, characters[p3CharChoice].type);
                    return characters[p3CharChoice].sprite;
                }
            case PlayerID.P4:
                if (p4CharChoice + 1 == characters.Length)
                {
                    p4CharChoice = 0;
                    globalManager.SetPlayerCharacter(PlayerID.P4, characters[0].type);
                    return characters[0].sprite;
                }
                else
                {
                    p4CharChoice++;
                    globalManager.SetPlayerCharacter(PlayerID.P4, characters[p4CharChoice].type);
                    return characters[p4CharChoice].sprite;
                }
        }
        return null;
    }

    public Sprite GetLeftCharacterSprite(PlayerID player)
    {
        switch (player)
        {
            case PlayerID.P1:
                if (p1CharChoice - 1 < 0)
                {
                    p1CharChoice = characters.Length - 1;
                    globalManager.SetPlayerCharacter(PlayerID.P1, characters[p1CharChoice].type);
                    return characters[p1CharChoice].sprite;
                }
                else
                {
                    p1CharChoice--;
                    globalManager.SetPlayerCharacter(PlayerID.P1, characters[p1CharChoice].type);
                    return characters[p1CharChoice].sprite;
                }
            case PlayerID.P2:
                if (p2CharChoice - 1 < 0)
                {
                    p2CharChoice = characters.Length - 1;
                    globalManager.SetPlayerCharacter(PlayerID.P2, characters[p2CharChoice].type);
                    return characters[p2CharChoice].sprite;
                }
                else
                {
                    p2CharChoice--;
                    globalManager.SetPlayerCharacter(PlayerID.P2, characters[p2CharChoice].type);
                    return characters[p2CharChoice].sprite;
                }
            case PlayerID.P3:
                if (p3CharChoice - 1 < 0)
                {
                    p3CharChoice = characters.Length - 1;
                    globalManager.SetPlayerCharacter(PlayerID.P3, characters[p3CharChoice].type);
                    return characters[p3CharChoice].sprite;
                }
                else
                {
                    p3CharChoice--;
                    globalManager.SetPlayerCharacter(PlayerID.P3, characters[p3CharChoice].type);
                    return characters[p3CharChoice].sprite;
                }
            case PlayerID.P4:
                if (p4CharChoice - 1 < 0)
                {
                    p4CharChoice = characters.Length - 1;
                    globalManager.SetPlayerCharacter(PlayerID.P4, characters[p4CharChoice].type);
                    return characters[p4CharChoice].sprite;
                }
                else
                {
                    p4CharChoice--;
                    globalManager.SetPlayerCharacter(PlayerID.P4, characters[p4CharChoice].type);
                    return characters[p4CharChoice].sprite;
                }
        }
        return null;
    }

    private void Start()
    {
        globalManager = GameObject.FindGameObjectWithTag("Global").GetComponent<GlobalManager>();
    }

    private void Update()
    {
        if (playersReady >= 2)
        {
            startPanel.enabled = true;
            readyToStart = true;
        }

        if (Input.GetButtonDown("A_1"))
        {
            if (p1Playing && readyToStart) // selected character
            {
                StartGame();
            }
            else if (p1Playing && !p1Ready)
            {
                p1Ready = true;
                playersReady++;
                p1ReadyLabel.enabled = true;
                p1ButtonSprite.enabled = false;
            }
            else if (!p1Playing)
            {
                p1Playing = true;
                p1CharSprite.enabled = true;
                globalManager.PlayerAdded(PlayerID.P1);
            }
        }
        if (Input.GetButtonDown("A_2"))
        {
            if (p2Playing && readyToStart)
            {
                StartGame();
            }

            else if (p2Playing && !p2Ready)
            {
                p2Ready = true;
                playersReady++;
                p2ReadyLabel.enabled = true;
                p2ButtonSprite.enabled = false;
            }
            else if (!p2Playing)
            {
                p2Playing = true;
                p2CharSprite.enabled = true;
                globalManager.PlayerAdded(PlayerID.P2);
            }
        }
        if (Input.GetButtonDown("A_3"))
        {
            if (p3Playing && readyToStart)
            {
                StartGame();
            }
            else if (p3Playing && !p3Ready)
            {
                p3Ready = true;
                playersReady++;
                p3ReadyLabel.enabled = true;
                p3ButtonSprite.enabled = false;
            }
            else if (!p3Playing)
            {
                p3Playing = true;
                p3CharSprite.enabled = true;
                globalManager.PlayerAdded(PlayerID.P3);
            }
        }
        if (Input.GetButtonDown("A_4"))
        {
            if (p4Playing && readyToStart)
            {
                StartGame();
            }
            else if (p4Playing && !p4Ready)
            {
                p4Ready = true;
                playersReady++;
                p4ReadyLabel.enabled = true;
                p4ButtonSprite.enabled = false;
            }
            else if (!p4Playing)
            {
                p4Playing = true;
                p4CharSprite.enabled = true;
                globalManager.PlayerAdded(PlayerID.P4);
            }
        }

        // left buttons
        if (!p1Swapped && p1Playing && !p1Ready && Input.GetAxisRaw("L_XAxis_1") < 0)
        {
            p1CharSprite.sprite2D = GetLeftCharacterSprite(PlayerID.P1);
            p1Swapped = true;
        }
        if (!p2Swapped && p2Playing && !p2Ready && Input.GetAxisRaw("L_XAxis_2") < 0)
        {
            p2CharSprite.sprite2D = GetLeftCharacterSprite(PlayerID.P2);
            p2Swapped = true;
        }
        if (!p3Swapped && p3Playing && !p3Ready && Input.GetAxisRaw("L_XAxis_3") < 0)
        {
            p3CharSprite.sprite2D = GetLeftCharacterSprite(PlayerID.P3);
            p3Swapped = true;
        }
        if (!p4Swapped && p4Playing && !p4Ready && Input.GetAxisRaw("L_XAxis_4") < 0)
        {
            p4CharSprite.sprite2D = GetLeftCharacterSprite(PlayerID.P4);
            p4Swapped = true;
        }

        // right buttons
        if (!p1Swapped && p1Playing && !p1Ready && Input.GetAxisRaw("L_XAxis_1") > 0)
        {
            p1CharSprite.sprite2D = GetRightCharacterSprite(PlayerID.P1);
            p1Swapped = true;
        }
        if (!p2Swapped && p2Playing && !p2Ready && Input.GetAxisRaw("L_XAxis_2") > 0)
        {
            p2CharSprite.sprite2D = GetRightCharacterSprite(PlayerID.P2);
            p2Swapped = true;
        }
        if (!p3Swapped && p3Playing && !p3Ready && Input.GetAxisRaw("L_XAxis_3") > 0)
        {
            p3CharSprite.sprite2D = GetRightCharacterSprite(PlayerID.P3);
            p3Swapped = true;
        }
        if (!p4Swapped && p4Playing && !p4Ready && Input.GetAxisRaw("L_XAxis_4") > 0)
        {
            p4CharSprite.sprite2D = GetRightCharacterSprite(PlayerID.P4);
            p4Swapped = true;
        }

        if (Input.GetAxisRaw("L_XAxis_1") == 0) p1Swapped = false;
        if (Input.GetAxisRaw("L_XAxis_2") == 0) p2Swapped = false;
        if (Input.GetAxisRaw("L_XAxis_3") == 0) p3Swapped = false;
        if (Input.GetAxisRaw("L_XAxis_4") == 0) p4Swapped = false;
    }

    private void StartGame()
    {
        Application.LoadLevel("game");
    }
}