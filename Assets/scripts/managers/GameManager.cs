﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour
{
    private GlobalManager globalManager;
    private WandSpawnManager wandSpawnManager;
    public Transform p1Spawn, p2Spawn, p3Spawn, p4Spawn;
    public GameObject playerPrefab;
    private float currentTime;
    public float roundWaitTime;
    public int PlayerCount { get; private set; }
    private bool isRoundReady, isRoundStarted;
    private bool p1Alive, p2Alive, p3Alive, p4Alive;
    private GameUIManager gameUiManager;

    private void Start()
    {
        globalManager = GameObject.FindGameObjectWithTag("Global").GetComponent<GlobalManager>();
        wandSpawnManager = GameObject.FindGameObjectWithTag("WandSpawnManager").GetComponent<WandSpawnManager>();
        gameUiManager = GetComponent<GameUIManager>();

        SpawnPlayers();
        SetPlayerCount();
        StartRound();
    }

    private void SpawnPlayers()
    {
        if (globalManager.IsPlayerActive(PlayerID.P1))
        {
            var player = Instantiate(playerPrefab, p1Spawn.position, Quaternion.identity) as GameObject;
            player.GetComponent<Animator>().SetTrigger(globalManager.GetTriggerStringForCharacterSelection(PlayerID.P1));
            player.BroadcastMessage("SetPlayer", PlayerID.P1);
            player.BroadcastMessage("SetPlayer", globalManager.GetPlayerCharacter(PlayerID.P1));
            p1Alive = true;
        }
        if (globalManager.IsPlayerActive(PlayerID.P2))
        {
            var player = Instantiate(playerPrefab, p2Spawn.position, Quaternion.identity) as GameObject;
            player.GetComponent<Animator>().SetTrigger(globalManager.GetTriggerStringForCharacterSelection(PlayerID.P2));
            player.BroadcastMessage("SetPlayer", PlayerID.P2);
            player.BroadcastMessage("SetPlayer", globalManager.GetPlayerCharacter(PlayerID.P2));
            p2Alive = true;
        }
        if (globalManager.IsPlayerActive(PlayerID.P3))
        {
            var player = Instantiate(playerPrefab, p3Spawn.position, Quaternion.identity) as GameObject;
            player.GetComponent<Animator>().SetTrigger(globalManager.GetTriggerStringForCharacterSelection(PlayerID.P3));
            player.BroadcastMessage("SetPlayer", PlayerID.P3);
            player.BroadcastMessage("SetPlayer", globalManager.GetPlayerCharacter(PlayerID.P3));
            p3Alive = true;
        }
        if (globalManager.IsPlayerActive(PlayerID.P4))
        {
            var player = Instantiate(playerPrefab, p4Spawn.position, Quaternion.identity) as GameObject;
            player.GetComponent<Animator>().SetTrigger(globalManager.GetTriggerStringForCharacterSelection(PlayerID.P4));
            player.BroadcastMessage("SetPlayer", PlayerID.P4);
            player.BroadcastMessage("SetPlayer", globalManager.GetPlayerCharacter(PlayerID.P4));
            p4Alive = true;
        }
    }

    private void SetPlayerCount()
    {
        PlayerCount = 0;
        if (p1Alive)
        {
            PlayerCount++;
        }
        if (p2Alive)
        {
            PlayerCount++;
        }
        if (p3Alive)
        {
            PlayerCount++;
        }
        if (p4Alive)
        {
            PlayerCount++;
        }
    }

    private void StartRound()
    {
        isRoundReady = true;
    }

    private void Update()
    {
        if (isRoundReady && (currentTime += Time.deltaTime) > roundWaitTime)
        {
            isRoundStarted = true;
            isRoundReady = false;
            wandSpawnManager.SpawnRoundStartWands();
        }
        if (isRoundStarted && PlayerCount == 1)
        {
            isRoundStarted = false;
            DeclareWinner();
        }
    }

    private void DeclareWinner()
    {
        PlayerID winnerID = FindWinner();
        gameUiManager.DeclareWinner(winnerID);
    }

    private PlayerID FindWinner()
    {
        if (p1Alive) return PlayerID.P1;
        if (p2Alive) return PlayerID.P2;
        if (p3Alive) return PlayerID.P3;
        if (p4Alive) return PlayerID.P4;
        return PlayerID.None;
    }

    public bool HasRoundStarted()
    {
        return isRoundStarted;
    }

    public bool IsPlayerAlive(PlayerID player)
    {
        switch (player)
        {
            case PlayerID.P1:
                return p1Alive;
            case PlayerID.P2:
                return p2Alive;
            case PlayerID.P3:
                return p3Alive;
            case PlayerID.P4:
                return p4Alive;
            default:
                return false;
        }
    }

    public void KillPlayer(PlayerID player)
    {
        switch (player)
        {
            case PlayerID.P1:
                p1Alive = false;
                break;
            case PlayerID.P2:
                p2Alive = false;
                break;
            case PlayerID.P3:
                p3Alive = false;
                break;
            case PlayerID.P4:
                p4Alive = false;
                break;
        }
        PlayerCount--;
    }

    public void RestartRound()
    {
        ResetGame();
        SpawnPlayers();
        SetPlayerCount();
        StartRound();
    }

    private void ResetGame()
    {
        // destroy players
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
        for (int i = 0; i < players.Length; i++)
        {
            Destroy(players[i]);
        }

        // destroy wands
        GameObject[] wands = GameObject.FindGameObjectsWithTag("Wand");
        for (int i = 0; i < wands.Length; i++)
        {
            Destroy(wands[i]);
        }
    }
}