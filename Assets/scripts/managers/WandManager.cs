﻿using UnityEngine;
using System.Collections;

public class WandManager : MonoBehaviour
{
    public SpriteRenderer sprite;
    public GameObject wandPrefab;
    private Movement movement;
    private WandType equippedWand;
    private int wandUses;

    private void Awake()
    {
        equippedWand = WandType.None;
    }

    private void Start()
    {
        movement = GetComponent<Movement>();
    }

    public bool IsWandEquipped()
    {
        return equippedWand != WandType.None;
    }

    public WandType GetWandType()
    {
        return equippedWand;
    }

    public void PickUpWand(Wand wand)
    {
        // we have no wand, pick it up
        if (!IsWandEquipped())
        {
            EquipWand(wand);
            Debug.Log("Equipped Wand");
        }
        else // we have wand, swap it out
        {
            ThrowWand();
            EquipWand(wand);
            Debug.Log("Threw and Equipped Wand");
        }
    }

    public void ThrowWand()
    {
        UnequipWand();
        GameObject wand = (GameObject) Instantiate(wandPrefab, transform.position, Quaternion.identity);
        wand.GetComponent<Wand>().GetThrown(movement.GetAimDirection(), movement.playerId);
    }

    private void EquipWand(Wand wand)
    {
        sprite.enabled = true;
        equippedWand = wand.Type;
        Destroy(wand.gameObject);
        wandUses = 3;

        GameObject wpm = GameObject.FindGameObjectWithTag("WandParticleManager");
        ParticleSystem p = wpm.GetComponent<WandParticleManager>().GetWandParticles(wand.Type);
        var go = (ParticleSystem)Instantiate(p, sprite.gameObject.transform.GetChild(0).position, p.transform.rotation);
        go.transform.parent = sprite.gameObject.transform;
        go.renderer.sortingLayerName = "Foreground";
    }

    private void UnequipWand()
    {
        sprite.enabled = false;
        equippedWand = WandType.None;
        Destroy(sprite.gameObject.transform.GetChild(1).gameObject);
    }

    // returns true if we can use wand
    public bool TryToUseWand()
    {
        wandUses--;
        if (wandUses == 0)
        {
            sprite.gameObject.transform.GetChild(1).particleSystem.Stop();
        }
        return wandUses >= 0;
    }
}