﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using System.Collections;

public class WandSpawnManager : MonoBehaviour
{
    public GameObject wandPrefab;
    public float wandSpawnRate;
    private float currentSpawnTime;
    private List<Transform> wandSpawnpoints;
    private List<Transform> initialWandSpawnpoints;
    private GameManager gameManager;

    private void Start()
    {
        gameManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
        GetSpawnpointTransforms();
    }

    private void GetSpawnpointTransforms()
    {
        wandSpawnpoints = new List<Transform>();
        initialWandSpawnpoints = new List<Transform>();

        foreach (Transform child in transform)
        {
            wandSpawnpoints.Add(child);
            if (child.CompareTag("WandSpawnInitial"))
            {
                initialWandSpawnpoints.Add(child);
            }
        }
    }

    public void SpawnRoundStartWands()
    {
        Vector3[] points = GetInitialWandSpawnPoints();
        for (int i = 0; i < points.Length; i++)
        {
            Instantiate(wandPrefab, points[i], Quaternion.identity);
        }
    }

    private Vector3[] GetInitialWandSpawnPoints()
    {
        Vector3[] points = new Vector3[3];
        int ignore = Random.Range(0, initialWandSpawnpoints.Count);
        int counter = 0;
        for (int i = 0; i < initialWandSpawnpoints.Count; i++)
        {
            if (i != ignore)
            {
                points[counter] = initialWandSpawnpoints[i].position;
                counter++;
            }
        }
        return points;
    }

    private Vector3 GetRandomWandSpawnPoint()
    {
        return wandSpawnpoints[Random.Range(0, wandSpawnpoints.Count)].position;
    }

    private void Update()
    {
        if (!gameManager.HasRoundStarted())
        {
            currentSpawnTime = 0;
        }

        if (gameManager.HasRoundStarted() && (currentSpawnTime += Time.deltaTime) >= wandSpawnRate)
        {
            currentSpawnTime = 0;
            Instantiate(wandPrefab, GetRandomWandSpawnPoint(), Quaternion.identity);
        }
    }
}