﻿using UnityEngine;
using System.Collections;

public class WandParticleManager : MonoBehaviour
{
    [System.Serializable]
    public class WandParticle
    {
        public WandType type;
        public ParticleSystem particleSystem;
    }

    public WandParticle[] particles;

    public ParticleSystem GetWandParticles(WandType type)
    {
        for (int i = 0; i < particles.Length; i++)
        {
            if (particles[i].type == type)
                return particles[i].particleSystem;
        }
        return null; 
    }
}