﻿using System;
using UnityEngine;
using System.Collections.Generic;

public class GlobalManager : MonoBehaviour
{
    public List<PlayerID> activePlayers;
    public Characters p1Character, p2Character, p3Character, p4Character;

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
        Physics2D.IgnoreLayerCollision(LayerMask.NameToLayer("Firewalking"), LayerMask.NameToLayer("Firewalking"));
    }

    public void PlayerAdded(PlayerID player)
    {
        activePlayers.Add(player);
    }

    public bool IsPlayerActive(PlayerID player)
    {
        return activePlayers.Contains(player);
    }

    public void SetPlayerCharacter(PlayerID player, Characters character)
    {
        switch (player)
        {
            case PlayerID.P1:
                p1Character = character;
                break;
            case PlayerID.P2:
                p2Character = character;
                break;
            case PlayerID.P3:
                p3Character = character;
                break;
            case PlayerID.P4:
                p4Character = character;
                break;
        }
    }

    public Characters GetPlayerCharacter(PlayerID player)
    {
        switch (player)
        {
            case PlayerID.P1:
                return p1Character;
            case PlayerID.P2:
                return p2Character; 
            case PlayerID.P3:
                return p3Character; 
            case PlayerID.P4:
                return p4Character;
        }
        return Characters.Green; // not good!
    }

    public string GetTriggerStringForCharacterSelection(PlayerID player)
    {
        switch (player)
        {
            case PlayerID.P1:
                return GetTriggerStringFromCharacter(p1Character);
            case PlayerID.P2:
                return GetTriggerStringFromCharacter(p2Character);
            case PlayerID.P3:
                return GetTriggerStringFromCharacter(p3Character); 
            case PlayerID.P4:
                return GetTriggerStringFromCharacter(p4Character);
        }
        return "";
    }

    private string GetTriggerStringFromCharacter(Characters character)
    {
        switch (character)
        {
            case Characters.Blue:
                return "PlayerBlue";
            case Characters.Brown:
                return "PlayerBrown";
            case Characters.Green:
                return "PlayerGreen";
            case Characters.Red:
                return "PlayerRed";
            case Characters.Undead:
                return "PlayerUndead";
        }
        return "";
    }
}