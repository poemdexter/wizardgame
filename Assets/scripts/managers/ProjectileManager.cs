﻿using UnityEngine;
using System.Collections;

public class ProjectileManager : MonoBehaviour
{
    [System.Serializable]
    public class Projectile
    {
        public WandType type;
        public GameObject projectile;
    }

    public Projectile[] projectiles;

    public GameObject GetProjectile(WandType type)
    {
        for (int i = 0; i < projectiles.Length; i++)
        {
            if (projectiles[i].type == type)
                return projectiles[i].projectile;
        }
        return null;
    }
}