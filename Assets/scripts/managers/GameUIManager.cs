﻿using UnityEngine;
using System.Collections;

public class GameUIManager : MonoBehaviour
{
    public UILabel winnerLabel;
    public UILabel pushButtonLabel;
    private bool pushButtonWaiting;
    public float pushButtonWaitTime;
    private float pushButtonCurrentTime;
    private bool pushButtonEnabled;
    private GameManager gameManager;

    private void Start()
    {
        gameManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
    }

    public void DeclareWinner(PlayerID player)
    {
        winnerLabel.text = "Winner is Player " + (int) player;
        winnerLabel.enabled = true;
        pushButtonWaiting = true;
    }

    private void Update()
    {
        if (pushButtonWaiting && (pushButtonCurrentTime += Time.deltaTime) > pushButtonWaitTime)
        {
            pushButtonWaiting = false;
            pushButtonCurrentTime = 0;
            pushButtonLabel.enabled = true;
            pushButtonEnabled = true;
        }

        if (pushButtonEnabled && HasButtonBeenPushed())
        {
            winnerLabel.enabled = false;
            pushButtonLabel.enabled = false;
            pushButtonEnabled = false;
            gameManager.RestartRound();
        }
    }

    private bool HasButtonBeenPushed()
    {
        return Input.GetButtonDown("A_1") || Input.GetButtonDown("A_2") ||
               Input.GetButtonDown("A_3") || Input.GetButtonDown("A_3");
    }
}