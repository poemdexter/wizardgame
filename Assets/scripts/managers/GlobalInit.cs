﻿using UnityEngine;
using System.Collections;

public class GlobalInit : MonoBehaviour
{
    public GameObject globalPrefab;
    private void Awake()
    {
        if (GameObject.FindGameObjectWithTag("Global") == null)
        {
            Instantiate(globalPrefab);
        }
    }
}