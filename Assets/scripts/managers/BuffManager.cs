﻿using UnityEngine;
using System.Collections;

public class BuffManager : MonoBehaviour
{
    [System.Serializable]
    public class BuffRefs
    {
        public WandType type;
        public GameObject buff;
    }

    public BuffRefs[] buffs;

    public GameObject GetBuff(WandType type)
    {
        for (int i = 0; i < buffs.Length; i++)
        {
            if (buffs[i].type == type)
                return buffs[i].buff;
        }
        return null;
    }
}