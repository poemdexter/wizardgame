﻿using UnityEngine;
using System.Collections;

public class TouchFireWalk : MonoBehaviour
{
    public PlayerID Player { get; set; }
    
    private void OnTriggerEnter2D(Collider2D col)
    {
        // only destroy bullet if player isn't shooter and has > 0 hp
        if (col.CompareTag("Player"))
        {
            Health hp = col.GetComponent<Health>();
            if (hp != null && hp.playerId != Player && hp.HP > 0)
            {
                col.GetComponent<HandleGetHit>().InteractWithOthersBuff(WandType.Fire);
            }
        }
    }
}