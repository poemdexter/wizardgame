﻿using UnityEngine;
using System.Collections;

public class TitleScreenButtonBlink : MonoBehaviour
{
    public Sprite buttonUp, buttonDown;
    public float swapDelay;
    private UI2DSprite ui2DSprite;
    private float currentTime;
    private bool isUp = true;

    private void Start()
    {
        ui2DSprite = GetComponent<UI2DSprite>();
    }

    private void Update()
    {
        if ((currentTime += Time.deltaTime) > swapDelay)
        {
            isUp = !isUp;
            currentTime = 0;
            ui2DSprite.sprite2D = (isUp) ? buttonUp : buttonDown;
        }
    }
}