﻿using UnityEngine;
using System.Collections;

public class KillGameObjectAfterTime : MonoBehaviour
{
    public float killTime;
    private float currentTime;

    private void Update()
    {
        if ((currentTime += Time.deltaTime) >= killTime)
        {
            Destroy(gameObject);
        }
    }
}